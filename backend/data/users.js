import bcrypt from 'bcrypt' 

const users = [ 
    {
        name: 'admin user',
        email: 'admin@exemple.com',
        password: bcrypt.hashSync('admin', 10),
        isAdmin: true
    },
    {
        name: 'Joe Test',
        email: 'joe@exemple.com',
        password: bcrypt.hashSync('joe', 10),
        
    },
    {
        name: 'Mary Test',
        email: 'mary@exemple.com',
        password: bcrypt.hashSync('mary', 10),
    }
]

export default users