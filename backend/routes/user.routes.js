import express from 'express'
import {
    authUser,
    getUserProfile, 
    userRegistration, 
    updateUserProfile, 
    getUsers,
    deleteUser,
    getUserById,
    updateUser
} from '../controllers/user.controller.js'
import { protect, isAdmin } from '../middleware/authMiddleware.js';

const router = express.Router()

router.route('/profile')
    .get(protect, getUserProfile)
    .put(protect, updateUserProfile)
router.route('/registration').post(userRegistration)
router.route('/login').post(authUser)
router.route('/').get(protect, isAdmin, getUsers)
router.route('/:id/edit').put(protect, isAdmin, updateUser)
router.route('/:id')
    .get(protect, isAdmin, getUserById)
    .delete(protect, isAdmin, deleteUser)

export default router