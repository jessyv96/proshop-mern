import express from 'express'
import {
    addOrderItems,
    getOrderById,
    updateOrderToPay,
    getOrdersByUser,
    getOrders,
    updateOrderToDelivered
} from '../controllers/order.controller.js'
import { protect, isAdmin } from '../middleware/authMiddleware.js';

const router = express.Router()

router.route('/')
    .get(protect, isAdmin, getOrders)
    .post(protect, addOrderItems)
router.route('/myorders').get(protect, getOrdersByUser)
//Mettre les routes avec paramètre URL en dessous
router.route('/:id').get(protect, getOrderById)
router.route('/:id/pay').put(protect, updateOrderToPay)
router.route('/:id/deliver').put(protect, isAdmin, updateOrderToDelivered)

export default router