import express from 'express';
import dotenv from 'dotenv';
import colors from 'colors'
import morgan from 'morgan'
import path from 'path' 

import connectDB from './config/db.js'

import userRoutes from './routes/user.routes.js';
import productRoutes from './routes/product.routes.js'
import orderRoutes from './routes/order.routes.js'

const PORT = process.env.PORT || 5000
const __dirname = path.resolve() // dossier racine
const app = express()

app.disable('etag');
app.use(morgan('dev'))
app.use(express.json())

dotenv.config()

connectDB()

app.use('/api/users', userRoutes)
app.use('/api/orders', orderRoutes)
app.use('/api/products', productRoutes)

app.get('/api/config/paypal', (req, res) => {
    res.send(process.env.PAYPAL_CLIENT_ID)
})

if (process.env.NODE_ENV == 'prod') {
    app.use(express.static(path.join(__dirname, '/front-app/build')))

    app.get('*', function (req, res) {
        res.sendFile(path.join(__dirname, '/front-app/build', 'index.html'));
      });
}

app.listen(PORT, () => {
    console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.blue.bold)
})