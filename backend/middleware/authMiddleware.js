import jwt from 'jsonwebtoken'
import asyncHandler from 'express-async-handler'
import User from '../models/User.model.js'

const protect = asyncHandler(async (req, res, next) => {
    let token

    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        try {
            token = req.headers.authorization.split(' ')[1]
            const decoded = jwt.verify(token, process.env.JWT_SECRET_TOKEN)
            req.user = await User.findById(decoded.id).select('-password')
            next()
        } catch (error) {
            console.log(error)
            res.status(401)
            throw new Error('INVALID_TOKEN')
        }
    }

    if (!token) {
        res.status(401)
        throw new Error('NO_TOKEN')
    }
})

const isAdmin = asyncHandler(async (req, res, next) => {
    if(req.user && req.user.isAdmin) {
        next()
    } else {
        res.status(401)
        throw new Error('NOT_AUTHTORIZE_AS_ADMIN')
    }
})

export {
    protect,
    isAdmin
}