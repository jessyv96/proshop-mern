import asyncHandler from 'express-async-handler'
import User from '../models/User.model.js'
import generateToken from '../utils/generateToken.js'

/**
 * @desc    Auth User & Get token
 * @route   POST /api/users/login
 * @access  Public
 */
const authUser = asyncHandler(async (req, res) => {
    const { email, password } = req.body

    const user = await User.findOne({ email: email })
    if (user && await user.matchPassword(password)) {
        res.json({
            _id: user._id,
            name: user.name,
            email: user.email,
            isAdmin: user.isAdmin,
            token: generateToken(user._id)
        })
    } else {
        res.status(401)
        throw new Error('INVALID_EMAIL_OR PASSWORD')
    }
})

/**
 * @desc    User profile
 * @route   GET /api/users/profile
 * @access  Private
 */
const getUserProfile = asyncHandler(async (req, res) => {
    const user = await User.findById(req.user._id)

    if (user) {
        res.json({
            _id: user._id,
            name: user.name,
            email: user.email,
            isAdmin: user.isAdmin,
        })
    } else {
        res.status(404)
        throw new Error('USER_NOT_FOUND')
    }
})

/**
 * @desc    User Registration
 * @route   POST /api/users/registration
 * @access  Public
 */
const userRegistration = asyncHandler(async (req, res) => {
    const { name, email, password } = req.body

    const userExist = await User.findOne({ email })

    if (userExist) {
        res.status(401)
        throw new Error('USER_EXIST')
    }

    const user = await User.create({
        name,
        email,
        password
    })

    if (user) {
        res.status(201).json({
            _id: user._id,
            name: user.name,
            email: user.email,
            isAdmin: user.isAdmin,
            token: generateToken(user._id)
        })
    } else {
        res.status(400)
        throw new Error('INVALID_USER_DATA')
    }

})


/**
 * @desc    Update user profile
 * @route   PUT /api/users/profile
 * @access  Private
 */
const updateUserProfile = asyncHandler(async (req, res) => {
    const user = await User.findById(req.user._id)

    if (user) {
        user.name = req.body.name || user.name
        user.email = req.body.email || user.email
        if (req.body.password) {
            user.password = req.body.password || user.password
        }

        const updatedUser = await user.save()

        res.json({
            _id: updatedUser._id,
            name: updatedUser.name,
            email: updatedUser.email,
            isAdmin: updatedUser.isAdmin,
            token: generateToken(updatedUser._id)
        })
    } else {
        res.status(404)
        throw new Error('USER_NOT_FOUND')
    }
})

/**
 * @desc    Get all users
 * @route   PUT /api/users/
 * @access  Private/admin
 */
const getUsers = asyncHandler(async (req, res) => {
    const user = await User.find({}).select('-password')
    if(user) {
      res.json(user)  
    } else {
        res.status(404)
        throw new Error('USER_NOT_FOUND')
    }  
})

/**
 * @desc    Get user by id
 * @route   GET /api/users/:id
 * @access  Private/admin
 */
const getUserById = asyncHandler(async (req, res) => {
    const users = await User.findById(req.params.id)
    res.json(users)
})


/**
 * @desc    Delete a user
 * @route   DELETE /api/users/:id
 * @access  Private/admin
 */
const deleteUser = asyncHandler(async (req, res) => {
    const user = await User.findById(req.params.id)
    if(user) {
        user.remove()
        res.json({'message': `user ${req.params.id} removed`})
    } else {
        res.status(404)
        throw new Error('User not found')
    }

    res.json(users)
})

/**
 * @desc    Update user
 * @route   PUT /api/users/:id
 * @access  Private/admin
 */
const updateUser = asyncHandler(async (req, res) => {
    const user = await User.findById(req.params.id)

    if (user) {
        user.name = req.body.name || user.name
        user.email = req.body.email || user.email
        user.isAdmin = req.body.isAdmin || user.isAdmin

        const updatedUser = await user.save()

        res.json({
            _id: updatedUser._id,
            name: updatedUser.name,
            email: updatedUser.email,
            isAdmin: updatedUser.isAdmin,
        })
    } else {
        res.status(404)
        throw new Error('USER_NOT_FOUND')
    }
})

export {
    authUser,
    getUserProfile,
    userRegistration,
    updateUserProfile,
    getUsers,
    getUserById,
    deleteUser,
    updateUser
}
