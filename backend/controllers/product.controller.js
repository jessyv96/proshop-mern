import asyncHandler from 'express-async-handler'
import Product from '../models/Product.model.js'

/**
 * @desc    Fetch all products
 * @route   GET /api/products
 * @access  Public
 */
const getProducts = asyncHandler(async (req, res) => {
    const pageSize = 3;
    const page = Number(req.query.pageNumber) || 1
    const query = req.query.q ? {
        name: {
            $regex: req.query.q, //syntax mongoDB
            $options: 'i'
        }
    } : {}

    const count = await Product.countDocuments({ ...query })
    const products = await Product.find({ ...query }).limit(pageSize).skip(pageSize * (page - 1))

    res.status(200).json({ products, page, pages: Math.ceil(count / pageSize) })
})

/**
 * @desc    Fetch single product
 * @route   GET /api/products/:id
 * @access  Public
 */
const getProductById = asyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id)

    if (product) {
        return res.status(200).json(product)
    } else {
        return res.status(404).json({ message: 'PRODUCT_NOT_FOUND' })
    }
})

/**
 * @desc    Delete a product
 * @route   DELETE /api/products/:id
 * @access  Private/admin
 */
const deleteProduct = asyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id)

    if (product) {
        await product.remove()
        res.json({ message: "Product remove" })
    } else {
        return res.status(404).json({ message: 'PRODUCT_NOT_FOUND' })
    }
})

/**
 * @desc    Create a product
 * @route   POST /api/products
 * @access  Private/admin
 */
const createProduct = asyncHandler(async (req, res) => {
    const product = new Product({
        name: 'Playstation 5',
        price: 499.99,
        user: req.user._id,
        image: '/images/ps5.png',
        brand: 'Sony',
        category: 'Electronics',
        countInStock: 10,
        numReviews: 0,
        description: 'console de jeu'
    })

    const createProduct = await product.save()
    res.status(201).json(createProduct)
})

/**
 * @desc    Update a product
 * @route   PUT /api/products/:id
 * @access  Private/admin
 */
const updateProduct = asyncHandler(async (req, res) => {
    const {
        name,
        price,
        image,
        brand,
        description,
        countInStock,
        category } = req.body

    const product = await Product.findById(req.params.id)

    if (product) {
        product.name = name
        product.price = price
        product.description = description
        product.category = category
        product.brand = brand
        product.image = image
        product.countInStock = countInStock

        const updatedProduct = await product.save()

        res.json(updatedProduct)
    } else {
        res.status(404)
        throw new Error('PRODUCT_NOT_FOUND')
    }

})

/**
 * @desc    Create a new review
 * @route   PUT /api/products/:id/reviews
 * @access  Private
 */
const createProductReview = asyncHandler(async (req, res) => {
    const { rating, comment } = req.body

    const product = await Product.findById(req.params.id)

    if (product) {
        const alreadyReviews = product.reviews.find(review =>
            review.user.toString() === req.user._id.toString())
        if (alreadyReviews) {
            res.status(400)
            throw new Error('PRODUCT_ALREADY_REVIEWED')
        }
        const review = {
            name: req.user.name,
            rating: Number(rating),
            comment,
            user: req.user._id
        }

        product.reviews.push(review)
        product.numReviews = product.reviews.length
        product.rating = product.reviews.reduce((acc, item) => item.rating + acc, 0)
            / product.reviews.length

        await product.save()

        res.status(201).json({ message: "review added" })
    } else {
        res.status(404)
        throw new Error('PRODUCT_NOT_FOUND')
    }
})

/**
 * @desc    Get Top products
 * @route   GET /api/products/top
 * @access  Public
 */
const getTopProducts = asyncHandler(async (req, res) => {
    const products = await Product.find({}).sort({ rating: -1 }).limit(3)

    res.json(products)
})

export {
    getProducts,
    getProductById,
    deleteProduct,
    createProduct,
    updateProduct,
    createProductReview,
    getTopProducts
}