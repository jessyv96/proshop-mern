import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import {
    productListReducer,
    singleProductReducer,
    deleteProductReducer,
    createProductReducer,
    createReviewReducer,
    productTopRatedReducer
} from './reducers/product.reducer'
import { cartReducer } from './reducers/cart.reducer'
import { shippingReducer } from './reducers/shipping.reducer'
import { paymentReducer } from './reducers/payment.reducer'
import {
    orderCreateReducer,
    orderDetailsReducer,
    orderPayReducer,
    orderListReducer,
    orderAllReducer,
    orderDeliverReducer,
} from './reducers/order.reducer'
import {
    userLoginReducer,
    userRegisterReducer,
    userDetailsReducer,
    userUpdateProfileReducer,
    userListReducer,
    userDeleteReducer,
    userUpdateReducer
} from './reducers/user.reducer'

const reducer = combineReducers({
    productList: productListReducer,
    productTopRated: productTopRatedReducer,
    singleProduct: singleProductReducer,
    createProduct: createProductReducer,
    deleteProduct: deleteProductReducer,
    createReview: createReviewReducer,
    cart: cartReducer,
    shipping: shippingReducer,
    payment: paymentReducer,
    orderCreate: orderCreateReducer,
    orderDetails: orderDetailsReducer,
    orderPay: orderPayReducer,
    orderDeliver: orderDeliverReducer,
    orderListMy: orderListReducer,
    orderAll: orderAllReducer,
    userLogin: userLoginReducer,
    userRegister: userRegisterReducer,
    userDetails: userDetailsReducer,
    userUpdateProfile: userUpdateProfileReducer,
    userList: userListReducer,
    userDelete: userDeleteReducer,
    userUpdate: userUpdateReducer

})

const cartItemsFromStorage = localStorage.getItem('cartItems')
    ? JSON.parse(localStorage.getItem('cartItems'))
    : []

const userFromStorage = localStorage.getItem('userInfo')
    ? JSON.parse(localStorage.getItem('userInfo'))
    : null

const shippingAddressFromStorage = localStorage.getItem('shippingAddress')
    ? JSON.parse(localStorage.getItem('shippingAddress'))
    : {}

const paymentMethodFromStorage = localStorage.getItem('paymentMethod')
    ? JSON.parse(localStorage.getItem('paymentMethod'))
    : ''

const initialState = {
    cart: {
        cartItems: cartItemsFromStorage,
        shippingAddress: shippingAddressFromStorage,
        paymentMethod: paymentMethodFromStorage
    },
    userLogin: {
        userInfo: userFromStorage
    },
}

const middlerware = [thunk]

const store = createStore(
    reducer,
    initialState,
    composeWithDevTools(
        applyMiddleware(...middlerware)
    )
)

export default store