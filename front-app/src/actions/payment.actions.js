import { SAVE_PAYMENT_METHOD } from '../constants/payment.constants';

export const savePaymentMethod = (data) =>  async (dispatch) => {
    dispatch({
        type: SAVE_PAYMENT_METHOD,
        payload: data,
    })

    localStorage.setItem('paymentMethod', JSON.stringify(data))
} 