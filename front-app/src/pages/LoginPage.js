import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Row, Col, Form, Button, } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import FormContainer from '../components/FormContainer'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { login } from '../actions/user.actions'

const LoginPage = ({ location, history }) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const dispatch = useDispatch()

    const userLogin = useSelector(state => state.userLogin)

    const { loading, error, userInfo } = userLogin

    const redirect = location.search ? location.search.split('=')[1] : '/'

    useEffect(() => {
        if (userInfo) {
            history.push(redirect)
        }
    }, [history, userInfo, redirect])

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(login(email, password))
    }

    return (
        <FormContainer>
            <h2>Connexion</h2>
            {error && <Message variant="danger">{error}</Message>}
            {loading && <Loader />}
            <Form onSubmit={submitHandler}>
                <Form.Group controlId='email'>
                    <Form.Label>Adresse e-mail</Form.Label>
                    <Form.Control
                        type='email'
                        value={email}
                        placeholder='Entrer votre email'
                        onChange={(e) => setEmail(e.target.value)}></Form.Control>
                </Form.Group>

                <Form.Group controlId='password'>
                    <Form.Label>Mot de passe</Form.Label>
                    <Form.Control
                        type='password'
                        value={password}
                        placeholder='Mot de passe'
                        onChange={(e) => setPassword(e.target.value)}></Form.Control>
                </Form.Group>

                <Button type="submit" variant='dark'>Connexion</Button>

                <Row className="py-3">
                    <Col>
                        Nouveau Client, <Link to={redirect ? `/register?redirect=${redirect}` : 'register'}>S'inscrire</Link>
                    </Col>
                </Row>
            </Form>
        </FormContainer>
    )
}

export default LoginPage
