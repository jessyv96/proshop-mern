import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom'
import { Row, Col, Image, ListGroup, Card, Button, Form } from 'react-bootstrap'
import Loader from '../components/Loader'
import Message from '../components/Message'
import Rating from '../components/Rating'
import Meta from '../components/Meta'
import { getProduct, productCreateReview } from '../actions/product.actions'
import { PRODUCT_CREATE_REVIEW_RESET } from '../constants/product.constants'

const ProductPage = ({ history, match }) => {
    const [quantity, setQuantity] = useState(1)
    const [rating, setRating] = useState(0)
    const [comment, setComment] = useState('')

    const dispatch = useDispatch();

    const createReview = useSelector(state => state.createReview)
    const { error: errorCreateReview, success: successCreateReview } = createReview

    useEffect(() => {
        if (successCreateReview) {
            alert('Merci')
            setRating(0)
            setComment('')
            dispatch({ type: PRODUCT_CREATE_REVIEW_RESET })
        }
        dispatch(getProduct(match.params.id))

    }, [dispatch, match, successCreateReview])

    const singleProduct = useSelector(state => state.singleProduct)
    const { loading, error, product } = singleProduct

    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

    const addToCart = () => {
        if (quantity > 0) {
            history.push(`/cart/${match.params.id}?quantity=${quantity}`)
        } else {
            console.log('Selectionnez au moins une unité')
        }
    }
    const submitHandler = (e) => {
        e.preventDefault()

        dispatch(productCreateReview(match.params.id, { rating, comment }))
    }

    return (
        <>
            <Meta title={product.name} />
            <Link className="btn btn-light my-3" to="/">Retour</Link>
            {loading ?
                <Loader />
                : error ?
                    <Message variant="danger" message={error.message} /> : (
                        <>
                            <Row>
                                <Col md={6}>
                                    <Image src={product.image} alt={product.name} fluid></Image>
                                </Col>
                                <Col md={3}>
                                    <ListGroup variant="flush">
                                        <ListGroup.Item>
                                            <h3>{product.name}</h3>
                                        </ListGroup.Item>
                                        <ListGroup.Item>
                                            <Rating value={product.rating} text={` vu ${product.numReviews} fois`} />
                                        </ListGroup.Item>
                                        <ListGroup.Item>
                                            Prix : {product.price}
                                        </ListGroup.Item>
                                        <ListGroup.Item>
                                            Description : {product.description}
                                        </ListGroup.Item>
                                    </ListGroup>
                                </Col>
                                <Col md={3}>
                                    <Card>
                                        <ListGroup>
                                            <ListGroup.Item>
                                                Prix : {product.price}
                                            </ListGroup.Item>
                                            <ListGroup.Item>
                                                Status : <span style={product.countInStock > 0 ? { color: 'green' } : { color: 'red' }}>{product.countInStock > 0 ? 'Disponible' : 'Indisponible'}</span>
                                            </ListGroup.Item>
                                            {product.countInStock > 0 && (
                                                <ListGroup.Item>
                                                    <Row>
                                                        <Col>Quantité :</Col>
                                                        <Col>
                                                            <Form.Control
                                                                as="select"
                                                                value={quantity}
                                                                onChange={(e) => setQuantity(e.target.value)}
                                                            >
                                                                {[...Array(product.countInStock).keys()].map(x => (
                                                                    <option key={x + 1} value={x + 1}>
                                                                        {x + 1}
                                                                    </option>
                                                                ))}
                                                            </Form.Control>
                                                        </Col>
                                                    </Row>
                                                </ListGroup.Item>
                                            )}
                                            <ListGroup.Item>
                                                <Button
                                                    onClick={() => addToCart()}
                                                    className="btn btn-dark btn-block"
                                                    disabled={product.countInStock === 0}>
                                                    Ajouter au panier
                                        </Button>
                                            </ListGroup.Item>
                                        </ListGroup>
                                    </Card>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6}>
                                    <h2>Commentaires</h2>
                                    {errorCreateReview && <Message variant='danger'>{errorCreateReview}</Message>}
                                    {product !== {} && product.reviews && (
                                        <ListGroup>
                                            {product.reviews.length === 0 ? <Message>pas d'avis</Message> :
                                                product.reviews.map(review => (
                                                    <ListGroup.Item key={review._id}>
                                                        <strong>{review.name}</strong>
                                                        <Rating value={review.rating} />
                                                        <p>{review.createdAt.substring(0, 10)}</p>
                                                        <p>{review.comment}</p>
                                                    </ListGroup.Item>
                                                ))}
                                            <ListGroup.Item>
                                                <h2>Laisser un commentaire</h2>
                                                {userInfo ? (
                                                    <Form onSubmit={submitHandler}>
                                                        <Form.Group controlId='rating'>
                                                            <Form.Label>Note</Form.Label>
                                                            <Form.Control as='select' value={rating} onChange={(e) => setRating(e.target.value)}>
                                                                <option value=''>Choisissez :</option>
                                                                <option value='1'>1 - Très faible</option>
                                                                <option value='2'>2 - Faible</option>
                                                                <option value='3'>3 - Moyen</option>
                                                                <option value='4'>4 - Bien</option>
                                                                <option value='5'>5 - Très bien</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                        <Form.Group controlId='comment'>
                                                            <Form.Label>Commentaire</Form.Label>
                                                            <Form.Control as='textarea' value={comment} onChange={(e) => setComment(e.target.value)}>

                                                            </Form.Control>
                                                        </Form.Group>
                                                        <Button type='submit' variant='dark'>Commenter</Button>
                                                    </Form>) :
                                                    <Message><Link to='/login'>Connectez-vous</Link>, pour laisser un commentaire</Message>}
                                            </ListGroup.Item>
                                        </ListGroup>)}
                                </Col>
                            </Row>
                        </>
                    )
            }

        </>
    )
}

export default ProductPage
