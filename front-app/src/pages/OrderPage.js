import { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Row, Col, ListGroup, Button, Image, Card } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import CheckoutSteps from '../components/CheckoutSteps'
import Message from '../components/Message'
import { createOrder } from '../actions/order.actions';
import { getItemsPrice } from '../services/item.service'

const OrderPage = ({ history }) => {
    const dispatch = useDispatch()

    const cart = useSelector(state => state.cart)
    const { cartItems, shippingAddress, paymentMethod } = cart

    const addDecimals = (num) => {
        return (Math.round(num * 100) / 100).toFixed(2)
    }

    const orderCreate = useSelector(state => state.orderCreate)
    const { error, order, success } = orderCreate

    useEffect(() => {
        if (success) {
            history.push(`/order/${order._id}`)
        }
    }, [history, success, order])

    cart.itemsPrice = getItemsPrice(cartItems)
    cart.shippingPrice = addDecimals(cart.itemsPrice < 150 ? 9.99 : 0)
    cart.taxPrice = addDecimals((0.2 * cart.itemsPrice))
    cart.totalHT = addDecimals(Number(cart.itemsPrice) + Number(cart.shippingPrice))
    cart.totalPrice = addDecimals(Number(cart.itemsPrice) + Number(cart.shippingPrice) + Number(cart.taxPrice))

    const orderHandler = () => {
        dispatch(createOrder({
            orderItems: cartItems,
            shippingAddress,
            paymentMethod,
            itemsPrice: cart.itemsPrice,
            shippingPrice: cart.shippingPrice,
            taxPrice: cart.taxPrice,
            totalPrice: cart.totalPrice
        }))
    }
    return (
        <>
            <CheckoutSteps step1 step2 step3 step4></CheckoutSteps>
            <Row>
                <Col md={8}>
                    <ListGroup variant="flush">
                        <ListGroup.Item>
                            <h3>Livraison</h3>
                            <p>
                                <strong>Adresse :  </strong>
                                {shippingAddress.address},{' '}{shippingAddress.city}{' '}
                                {shippingAddress.postalCode},{' '}{shippingAddress.country}
                            </p>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <h3>Méthode de paiement</h3>
                            <strong>Methode : </strong>
                            {paymentMethod}
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <h3>Article(s) commandé(s)</h3>
                            {
                                cartItems.length === 0 ?
                                    <Message>Votre panier est vide</Message> :
                                    (
                                        <ListGroup variant='flush'>
                                            {
                                                cartItems.map((item, index) => (
                                                    <ListGroup.Item key={index}>
                                                        <Row>
                                                            <Col md={1}>
                                                                <Image
                                                                    src={item.image}
                                                                    alt={item.name}
                                                                    fluid
                                                                    rounded />
                                                            </Col>
                                                            <Col>
                                                                <Link to={`/product/${item.product_id}`}>{item.name}</Link>
                                                            </Col>
                                                            <Col md={4}>
                                                                {item.quantity} x {item.price} € = {item.quantity * item.price} €
                                                            </Col>
                                                        </Row>
                                                    </ListGroup.Item>
                                                ))
                                            }
                                        </ListGroup>
                                    )
                            }
                        </ListGroup.Item>
                    </ListGroup>
                </Col>
                <Col md={4}>
                    <Card>
                        <ListGroup variant="flush">
                            <ListGroup.Item>
                                <h3>Résumé de la Commande</h3>
                            </ListGroup.Item>
                            <ListGroup.Item>
                                <Row>
                                    <Col>Article(s) : </Col>
                                    <Col className="text-right">{cart.itemsPrice} €</Col>
                                </Row>
                                <Row>
                                    <Col>Livraison : </Col>
                                    <Col className="text-right">{cart.shippingPrice} €</Col>
                                </Row>
                                <Row>
                                    <Col>Total HT : </Col>
                                    <Col className="text-right">{cart.totalHT} €</Col>
                                </Row>
                                <Row>
                                    <Col>TVA : </Col>
                                    <Col className="text-right">{cart.taxPrice} €</Col>
                                </Row>
                                <Row>
                                    <Col>Prix Total :</Col>
                                    <Col className="text-right">{cart.totalPrice} €</Col>
                                </Row>
                            </ListGroup.Item>
                            <ListGroup.Item>
                                {error && <Message variant='danger'>{error}</Message>}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                <Button
                                    type="button"
                                    block
                                    variant="dark"
                                    disabled={cartItems.length === 0}
                                    onClick={orderHandler}>Commander</Button>
                            </ListGroup.Item>
                        </ListGroup>
                    </Card>

                </Col>
            </Row>
        </>
    )
}

export default OrderPage
