import { useState } from 'react'
import { Form, Button, } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import FormContainer from '../components/FormContainer'
import CheckoutSteps from '../components/CheckoutSteps'
import { saveShippingAddress } from '../actions/shipping.actions'


function ShippingPage({ history }) {
    const dispatch = useDispatch()
    const cart = useSelector(state => state.cart)
    const { shippingAddress } = cart

    const [address, setAddress] = useState(shippingAddress.address)
    const [city, setCity] = useState(shippingAddress.city)
    const [postalCode, setPostalCode] = useState(shippingAddress.postalCode)
    const [country, setCountry] = useState(shippingAddress.country)

    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(saveShippingAddress({address, city, postalCode, country}))
        history.push('/payment')
    }

    return (
        <FormContainer>
            <CheckoutSteps step1 step2 />
            <h2>Livraison</h2>
            <Form onSubmit={submitHandler}>
                <Form.Group controlId='address'>
                    <Form.Label>Adresse</Form.Label>
                    <Form.Control
                        type='text'
                        value={address}
                        placeholder='Entrer votre adresse'
                        required
                        onChange={(e) => setAddress(e.target.value)}></Form.Control>
                </Form.Group>
                <Form.Group controlId='city'>
                    <Form.Label>Ville</Form.Label>
                    <Form.Control
                        type='text'
                        value={city}
                        placeholder='Entrer votre ville'
                        required
                        onChange={(e) => setCity(e.target.value)}></Form.Control>
                </Form.Group>
                <Form.Group controlId='postalCode'>
                    <Form.Label>Code postal</Form.Label>
                    <Form.Control
                        type='text'
                        value={postalCode}
                        placeholder='Entrer votre code postal'
                        required
                        onChange={(e) => setPostalCode(e.target.value)}></Form.Control>
                </Form.Group>
                <Form.Group controlId='country'>
                    <Form.Label>Pays</Form.Label>
                    <Form.Control
                        type='text'
                        value={country}
                        placeholder='Entrer votre Pays'
                        required
                        onChange={(e) => setCountry(e.target.value)}></Form.Control>
                </Form.Group>
                <Button type="submit" variant="dark">Continuer</Button>
            </Form>
        </FormContainer>
    )
}

export default ShippingPage
