import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Form, Button, } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import FormContainer from '../../components/FormContainer'
import Message from '../../components/Message'
import Loader from '../../components/Loader'
import { getProduct } from '../../actions/product.actions'


const ProductEditPage = ({ history, match }) => {
    const productID = match.params.id
    const [name, setName] = useState('')
    const [price, setPrice] = useState(0)
    const [image, setImage] = useState('')
    const [description, setDescription] = useState('')
    const [brand, setBrand] = useState('')
    const [category, setCategory] = useState('')
    const [countInStock, setCountInStock] = useState(0)

    const dispatch = useDispatch()

    const singleProduct = useSelector(state => state.singleProduct)
    const { loading, error, product: productDetails } = singleProduct

    useEffect(() => {
        if (!productDetails || productDetails._id !== productID) {
            dispatch(getProduct(productID))
        }

    }, [dispatch, productDetails, productID])

    

    const submitHandler = (e) => {
        e.preventDefault()
        console.log('update product')
    }
    return (
        <>
            <Link to='/admin/products'>
                <Button className='btn btn-light my-3'>Retour</Button>
            </Link>
            <FormContainer>
                <h2>Modification Produit {productID}</h2>

                {loading ? <Loader /> : error ? (
                    <Message variant="danger">{error}</Message>) : (
                        <Form onSubmit={submitHandler}>
                            <Form.Group controlId='name'>
                                <Form.Label>Nom</Form.Label>
                                <Form.Control
                                    type='text'
                                    value={name}
                                    placeholder='Entrer votre nom'
                                    onChange={(e) => setName(e.target.value)}></Form.Control>
                            </Form.Group>
                            <Form.Group controlId='price'>
                                <Form.Label>Prix</Form.Label>
                                <Form.Control
                                    type='number'
                                    value={price}
                                    placeholder='Entrer le prix'
                                    onChange={(e) => setPrice(e.target.value)}></Form.Control>
                            </Form.Group>
                            <Form.Group controlId='description'>
                                <Form.Label>Prix</Form.Label>
                                <Form.Control
                                    type='text'
                                    value={description}
                                    placeholder='Décriver le produit'
                                    onChange={(e) => setDescription(e.target.value)}></Form.Control>
                            </Form.Group>
                            <Form.Group controlId='brand'>
                                <Form.Label>Marque</Form.Label>
                                <Form.Control
                                    type='text'
                                    value={brand}
                                    placeholder='La marque'
                                    onChange={(e) => setBrand(e.target.value)}></Form.Control>
                            </Form.Group>
                            <Form.Group controlId='category'>
                                <Form.Label>Catégorie</Form.Label>
                                <Form.Control
                                    type='text'
                                    value={category}
                                    placeholder='Catégorie'
                                    onChange={(e) => setCategory(e.target.value)}></Form.Control>
                            </Form.Group>
                            <Form.Group controlId='countInStock'>
                                <Form.Label>Stock</Form.Label>
                                <Form.Control
                                    type='number'
                                    value={countInStock}
                                    placeholder='Nombre de produit'
                                    onChange={(e) => setCountInStock(e.target.value)}></Form.Control>
                            </Form.Group>
                            <Form.Group controlId='image'>
                                <Form.Label>Photo</Form.Label>
                                <Form.Control
                                    type='text'
                                    value={image}
                                    placeholder='photo'
                                    onChange={(e) => setImage(e.target.value)}></Form.Control>
                            </Form.Group>

                            <Button type="submit" variant='dark' onClick={(e) => submitHandler(e)}>Modifier</Button>
                        </Form>
                    )}
            </FormContainer>
        </>
    )
}

export default ProductEditPage
