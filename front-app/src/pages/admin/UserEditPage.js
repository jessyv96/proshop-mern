import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Form, Button, } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import FormContainer from '../../components/FormContainer'
import Message from '../../components/Message'
import Loader from '../../components/Loader'
import { getUserDetails, updateUser } from '../../actions/user.actions'
import { USER_UPDATE_RESET } from '../../constants/user.constants'


const UserEditPage = ({ history, match }) => {
    const userID = match.params.id
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [isAdmin, setIsAdmin] = useState(false)

    const dispatch = useDispatch()

    const userDetails = useSelector(state => state.userDetails)
    const { loading, error, user } = userDetails

    const userUpdate = useSelector(state => state.userUpdate)
    const { loading:loadingUpdate, error:errorUpdate, success:successUpdate } = userUpdate

    useEffect(() => {
        if (successUpdate) {
            dispatch({
                type: USER_UPDATE_RESET,
            })
            history.push('/admin/users')
        } else {
            if (!user || user._id !== userID) {
                dispatch(getUserDetails(userID))
            } else {
                setName(user.name)
                setEmail(user.email)
                setIsAdmin(user.isAdmin)
            }
        }


    }, [user, dispatch, userID, successUpdate, history])

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(updateUser({_id: userID, name, email, isAdmin}))
    }
    return (
        <>
            <Link to='/admin/users'>
                <Button className='btn btn-light my-3'>Retour</Button>
            </Link>
            <FormContainer>
                <h2>Modification Utilisateur {userID}</h2>
                {loadingUpdate && <Loader />}
                {errorUpdate && <Message variant="danger">{errorUpdate}</Message>}
                {loading ? <Loader /> : error ? (
                    <Message variant="danger">{error}</Message>) : (
                        <Form onSubmit={submitHandler}>
                            <Form.Group controlId='name'>
                                <Form.Label>Nom</Form.Label>
                                <Form.Control
                                    type='text'
                                    value={name}
                                    placeholder='Entrer votre nom'
                                    onChange={(e) => setName(e.target.value)}></Form.Control>
                            </Form.Group>
                            <Form.Group controlId='email'>
                                <Form.Label>Adresse e-mail</Form.Label>
                                <Form.Control
                                    type='email'
                                    value={email}
                                    placeholder='Entrer votre email'
                                    onChange={(e) => setEmail(e.target.value)}></Form.Control>
                            </Form.Group>
                            <Form.Group controlId='isAdmin'>
                                <Form.Check
                                    type='checkbox'
                                    label='Administrateur'
                                    checked={isAdmin}
                                    onChange={(e) => setIsAdmin(e.target.checked)}>
                                </Form.Check>

                            </Form.Group>
                            <Button type="submit" variant='dark' onClick={(e) => submitHandler(e)}>Modifier</Button>
                        </Form>
                    )}
            </FormContainer>
        </>
    )
}

export default UserEditPage
