import { useState, useEffect } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import { PayPalButton } from "react-paypal-button-v2";
import { Row, Col, ListGroup, Image, Card, Button } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import Loader from '../components/Loader'
import Message from '../components/Message'
import { detailsOrder, paymentOrder, deliverOrder } from '../actions/order.actions';
import { ORDER_PAY_RESET, ORDER_DELIVER_RESET } from '../constants/order.constants';

const OrderDetailsPage = ({ match, history }) => {
    const orderId = match.params.id
    const dispatch = useDispatch()
    const [sdkReady, setSdkReady] = useState(false)

    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

    const orderDetails = useSelector(state => state.orderDetails)
    const { loading, error, order } = orderDetails

    const orderPay = useSelector(state => state.orderPay)
    const { success: successPay } = orderPay

    const orderDeliver = useSelector(state => state.orderDeliver)
    const { loading: loadingDeliver, success: successDeliver } = orderDeliver

    useEffect(() => {
        if (!userInfo) {
            history.push('/login')
        }
        const addPaypalScript = async () => {
            const { data: clientId } = await axios.get('/api/config/paypal')
            const script = document.createElement('script')
            script.text = 'text/javascript'
            script.src = `https://www.paypal.com/sdk/js?client-id${clientId}`
            script.async = true
            script.load = () => {
                setSdkReady(true)
            }
            document.body.appendChild(script)
        }

        if (!order || successPay || successDeliver) {
            dispatch({ type: ORDER_PAY_RESET })
            dispatch({ type: ORDER_DELIVER_RESET })
            dispatch(detailsOrder(orderId))
        } else if (!order.isPaid) {
            if (window.paypal) {
                addPaypalScript()
            } else {
                setSdkReady(true)
            }
        }

    }, [order, orderId, successPay, successDeliver, dispatch, history, userInfo])

    const successPaymentHandler = (paymentResult) => {
        dispatch(paymentOrder(order._id, paymentResult))
    }
    const deliverHandler = () => {
        dispatch(deliverOrder(order))
    }

    return loading ? <Loader /> :
        error ? <Message variant='danger'>{error}</Message> :
            <>
                <h2>Commande {order._id}</h2>
                <Row>
                    <Col md={8}>
                        <ListGroup variant="flush">
                            <ListGroup.Item>
                                <h3>Livraison</h3>
                                <p>
                                    <strong>Name : </strong>{order.user.name}
                                </p>
                                <p>
                                    <strong>Adresse mail : </strong>
                                    <a href={`mailto:${order.user.email}`}>{order.user.email}</a>
                                </p>
                                <p>
                                    <strong>Adresse :  </strong>
                                    {order.shippingAddress.address},{' '}{order.shippingAddress.city}{' '}
                                    {order.shippingAddress.postalCode},{' '}{order.shippingAddress.country}
                                </p>
                                {order.isDelivered ?
                                    <Message variant='success'>Livrée le {order.deliveredAt}</Message> :
                                    <Message variant='danger'>Non Livrée</Message>
                                }
                            </ListGroup.Item>
                            <ListGroup.Item>
                                <h3>Méthode de paiement</h3>
                                <p>
                                    <strong>Methode : </strong>
                                    {order.paymentMethod}
                                </p>
                                {order.isPaid ?
                                    <Message variant='success'>Payée le {order.paidAt}</Message> :
                                    <Message variant='danger'>Non Payée</Message>
                                }
                            </ListGroup.Item>
                            <ListGroup.Item>
                                <h3>Article(s) commandé(s)</h3>
                                {
                                    order.orderItems.length === 0 ?
                                        <Message>Votre panier est vide</Message> :
                                        (
                                            <ListGroup variant='flush'>
                                                {
                                                    order.orderItems.map((item, index) => (
                                                        <ListGroup.Item key={index}>
                                                            <Row>
                                                                <Col md={1}>
                                                                    <Image
                                                                        src={item.image}
                                                                        alt={item.name}
                                                                        fluid
                                                                        rounded />
                                                                </Col>
                                                                <Col>
                                                                    <Link to={`/product/${item.product_id}`}>{item.name}</Link>
                                                                </Col>
                                                                <Col md={4}>
                                                                    {item.quantity} x {item.price} € = {item.quantity * item.price} €
                                                    </Col>
                                                            </Row>
                                                        </ListGroup.Item>
                                                    ))
                                                }
                                            </ListGroup>
                                        )
                                }
                            </ListGroup.Item>
                        </ListGroup>
                    </Col>
                    <Col md={4}>
                        <Card>
                            <ListGroup variant="flush">
                                <ListGroup.Item>
                                    <h3>Résumé de la Commande</h3>
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <Row>
                                        <Col>Article(s) : </Col>
                                        <Col className="text-right">{order.itemsPrice || 0} €</Col>
                                    </Row>
                                    <Row>
                                        <Col>Livraison : </Col>
                                        <Col className="text-right">{order.shippingPrice} €</Col>
                                    </Row>
                                    <Row>
                                        <Col>TVA : </Col>
                                        <Col className="text-right">{order.taxPrice} €</Col>
                                    </Row>
                                    <Row>
                                        <Col>Prix Total :</Col>
                                        <Col className="text-right">{order.totalPrice} €</Col>
                                    </Row>
                                </ListGroup.Item>
                                {error &&
                                    <ListGroup.Item>
                                        <Message variant='danger'>{error}</Message>
                                    </ListGroup.Item>
                                }
                                {!order.isPaid && (
                                    <ListGroup.Item>
                                        {!sdkReady ? <Loader /> :
                                            <PayPalButton
                                                amount={order.totalPrice}
                                                onSuccess={successPaymentHandler}
                                                responsive />
                                        }
                                    </ListGroup.Item>
                                )}
                                {loadingDeliver && <Loader />}

                                {userInfo && userInfo.isAdmin && order.isPaid && !order.isDelivered && (
                                    <Button
                                        type='button'
                                        className='btn btn-block'
                                        onClick={deliverHandler}>Marqué comme Livrée</Button>
                                )}
                            </ListGroup>
                        </Card>
                    </Col>
                </Row>
            </>
}

export default OrderDetailsPage
