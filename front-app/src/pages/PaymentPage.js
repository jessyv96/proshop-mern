import { useState } from 'react'
import { Col, Form, Button } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import FormContainer from '../components/FormContainer'
import CheckoutSteps from '../components/CheckoutSteps'
import { savePaymentMethod } from '../actions/payment.actions'


function PaymentPage({ history }) {
    const dispatch = useDispatch()
    const [paymentMethod, setPaymentMethod] = useState('PayPal')
    const cart = useSelector(state => state.cart)
    const { shippingAddress } = cart

    if (!shippingAddress) {
        history.push('/shipping')
    }

    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(savePaymentMethod(paymentMethod))
        history.push('/order')
    }

    return (
        <FormContainer>
            <CheckoutSteps step1 step2 step3 />
            <h2>Livraison</h2>
            <Form onSubmit={submitHandler}>
                <Form.Group>
                    <Form.Label as='legend'>Sélectionnez la méthode de paiement</Form.Label>
                </Form.Group>
                <Col>
                    <Form.Check
                        type="radio"
                        label='Paypal or Credit card'
                        id="PayPal"
                        name="paymentMethod"
                        value="PayPal"
                        checked
                        onChange={(e) => setPaymentMethod(e.target.value)}></Form.Check>
                    <Form.Check
                        type="radio"
                        label='Stripe'
                        id="Stripe"
                        name="paymentMethod"
                        value="Stripe"
                        onChange={(e) => setPaymentMethod(e.target.value)}></Form.Check>
                </Col>
                <Button className="my-3" type="submit" variant="dark">Continuer</Button>
            </Form>
        </FormContainer>
    )
}

export default PaymentPage