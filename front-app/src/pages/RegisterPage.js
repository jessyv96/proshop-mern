import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Row, Col, Form, Button, } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import FormContainer from '../components/FormContainer'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { register } from '../actions/user.actions'

const RegisterPage = ({ location, history }) => {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [message, setMessage] = useState(null)
    const dispatch = useDispatch()

    const userRegister = useSelector(state => state.userRegister)
    const { loading, error, userInfo } = userRegister

    const redirect = location.search ? location.search.split('=')[1] : '/'

    useEffect(() => {
        if (userInfo) {
            history.push(redirect)
        }
    }, [history, userInfo, redirect])

    const submitHandler = (e) => {
        e.preventDefault()
        if (password !== confirmPassword) {
            setMessage('Les mots de passe ne correspondent pas')
        } else {
            dispatch(register(name, email, password))
        }
    }

    return (
        <FormContainer>
            <h2>Connexion</h2>
            {message && <Message variant="warning">{ message }</Message>}
            {error && <Message variant="danger">{error}</Message>}
            {loading && <Loader />}
            <Form onSubmit={submitHandler}>
                <Form.Group controlId='name'>
                    <Form.Label>Nom</Form.Label>
                    <Form.Control
                        type='text'
                        value={name}
                        placeholder='Entrer votre nom'
                        onChange={(e) => setName(e.target.value)}></Form.Control>
                </Form.Group>
                <Form.Group controlId='email'>
                    <Form.Label>Adresse e-mail</Form.Label>
                    <Form.Control
                        type='email'
                        value={email}
                        placeholder='Entrer votre email'
                        onChange={(e) => setEmail(e.target.value)}></Form.Control>
                </Form.Group>

                <Form.Group controlId='password'>
                    <Form.Label>Mot de passe</Form.Label>
                    <Form.Control
                        type='password'
                        value={password}
                        placeholder='Mot de passe'
                        onChange={(e) => setPassword(e.target.value)}></Form.Control>
                </Form.Group>

                <Form.Group controlId='confirmPassword'>
                    <Form.Label>Confirmation mot de passe</Form.Label>
                    <Form.Control
                        type='password'
                        value={confirmPassword}
                        placeholder='Confirmation mot de passe'
                        onChange={(e) => setConfirmPassword(e.target.value)}></Form.Control>
                </Form.Group>

                <Button type="submit" variant='dark'>S'inscrire</Button>

                <Row className="py-3">
                    <Col>
                        Avez-vous un compte, <Link to={redirect ? `/login?redirect=${redirect}` : 'login'}>Se connecter</Link>
                    </Col>
                </Row>
            </Form>
        </FormContainer>
    )
}

export default RegisterPage
