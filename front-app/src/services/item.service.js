 const getItemsPrice = (arrItems = []) => {
    return arrItems.reduce((accumulator, item) =>
        accumulator + item.quantity * item.price, 0).toFixed(2)
}

const getItemsQuantity = (arrItems = []) => {
    return arrItems.reduce((accumulator, item) => 
        accumulator + item.quantity, 0)
}

export {
    getItemsPrice,
    getItemsQuantity
}