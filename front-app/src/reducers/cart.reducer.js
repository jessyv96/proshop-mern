import { CART_ADD_ITEM, CART_REMOVE_ITEM } from '../constants/cart.constants';

export const cartReducer = (state = { cartItems: [] }, action) => {
    switch (action.type) {
        case CART_ADD_ITEM:
            const item = action.payload
            const existItem = state.cartItems.some(cartItem => cartItem.product_id === item.product_id)

            if (!existItem) {
                return {
                    ...state,
                    cartItems: [...state.cartItems, item]
                }
            } else {
                return {
                    ...state,
                    cartItems: state.cartItems.map((cartItem) =>
                        cartItem.product_id === item.product_id ? item : cartItem)
                }
            }
        case CART_REMOVE_ITEM:
            return {
                ...state,
                cartItems: state.cartItems.filter(cartItem =>
                    cartItem.product_id !== action.payload)
            }
        default:
            return state
    }
}