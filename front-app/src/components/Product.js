import React from 'react'
import Rating from './Rating';
import { Card } from 'react-bootstrap'
import { Link } from "react-router-dom";



const Product = ({ product }) => {
    return (
        <Card className="my-3 p-3 rounded">
            <Link to={`/product/${product._id}`}>
                <Card.Img src={product.image} />
            </Link>
            <Link to={`/product/${product._id}`}>
                <Card.Body>
                    <Card.Title as="div">{product.name}</Card.Title>
                    
                        <Rating value={product.rating} text={` vu ${product.numReviews} fois`} />
                    
                    <Card.Text as="h3">{product.price}€</Card.Text>
                </Card.Body>
            </Link>
        </Card>
    )
}

export default Product
