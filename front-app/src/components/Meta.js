import {Helmet} from "react-helmet";

const Meta = ({title, description, keywords}) => {
    return (
        <Helmet>
             <title>{title}</title>
             <meta name="description" content={description} />
             <meta name="keywords" content={keywords} />
        </Helmet>
    )
}

Meta.defaultProps = {
    title: 'Bienvenue sur Proshop',
    description: 'La référence des produits electroniques',
    keywords: 'produits, electroniques, meilleurs'
}

export default Meta
