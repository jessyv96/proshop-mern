import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom'
import { Carousel, Image } from 'react-bootstrap'
import Loader from '../components/Loader'
import Message from '../components/Message'
import { topProductsAction } from '../actions/product.actions'

const ProductCarousel = () => {
    const dispatch = useDispatch()

    const productTopRated = useSelector(state => state.productTopRated)
    const { loading, error, products: topProducts } = productTopRated

    useEffect(() => {
        dispatch(topProductsAction())
    }, [dispatch])

    return loading ? <Loader /> :
        error ? <Message variant='danger'>{error}</Message> : (
            <Carousel pause='hover' className='bg-dark'>
                {topProducts.map(product => (
                    <Carousel.Item key={product._id}>
                        <Link to={`/product/${product._id}`}>
                            <Image src={product.image} alt={product.name} fluid />
                            <Carousel.Caption className='carousel-caption'>
                                <h3>{product.name} ({product.price} €)</h3>
                            </Carousel.Caption>
                        </Link>
                    </Carousel.Item>
                ))}
            </Carousel>
        )

}

export default ProductCarousel
