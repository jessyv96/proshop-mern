import React, { useState } from 'react'
import { Form, Button } from 'react-bootstrap'

const SearchBox = ({history}) => {
    const [keyword, setKeyword] = useState('')
    const submitHandler = (e) => {
        e.preventDefault();
        if(keyword.trim()) {
            history.push(`/search/${keyword}`)
        } else {
            history.push(`/`)
        }
    }
    return (
        <Form onSubmit={submitHandler} inline>
            <Form.Control 
                className=''
                type='text' 
                name='q' 
                value={keyword}
                onChange={e => setKeyword(e.target.value)}
                placeholder='Rechercher un produit...'></Form.Control>
            <Button 
                type='submit' 
                variant='dark'  
                >Rechercher</Button>
        </Form>
    )
}

export default SearchBox
