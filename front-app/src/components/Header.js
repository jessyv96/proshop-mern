import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { LinkContainer } from 'react-router-bootstrap'
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap'
import { Route } from "react-router-dom";
import SearchBox from '../components/SearchBox'
import { logout } from '../actions/user.actions'

const Header = () => {
    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin
    const dispatch = useDispatch();
    const logoutHandler = () => {
        dispatch(logout())
    }
    return (
        <header>
            <Navbar bg="dark" variant="dark" expand="lg" collapseOnSelect>
                <Container>
                    <LinkContainer to="/">
                        <Navbar.Brand>ProShop</Navbar.Brand>
                    </LinkContainer>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Route render={({history}) => <SearchBox history={history} />} />
                        <Nav className="ml-auto">
                            <LinkContainer to="/cart">
                                <Nav.Link><i className="fas fa-shopping-cart"></i> Panier</Nav.Link>
                            </LinkContainer>
                            {userInfo ?
                                <NavDropdown title={userInfo.name} id="usermane">
                                    <LinkContainer to='/profile'>
                                        <NavDropdown.Item>Mon profil</NavDropdown.Item>
                                    </LinkContainer>
                                    <NavDropdown.Item onClick={logoutHandler}>Déconnexion</NavDropdown.Item>
                                </NavDropdown>
                                : <LinkContainer to="/login">
                                    <Nav.Link><i className="fas fa-user"></i> Connexion</Nav.Link>
                                </LinkContainer>
                            }
                            {userInfo && userInfo.isAdmin && (
                                <NavDropdown title='Admin' id="admin">
                                    <LinkContainer to='/admin/users'>
                                        <NavDropdown.Item>Utilisateurs</NavDropdown.Item>
                                    </LinkContainer>
                                    <LinkContainer to='/admin/products'>
                                        <NavDropdown.Item>Produits</NavDropdown.Item>
                                    </LinkContainer>
                                    <LinkContainer to='/admin/orders'>
                                        <NavDropdown.Item>Commandes</NavDropdown.Item>
                                    </LinkContainer>
                                    <NavDropdown.Item onClick={logoutHandler}>Déconnexion</NavDropdown.Item>
                                </NavDropdown>
                            )}
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </header >
    )
}

export default Header
